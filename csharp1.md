# C# and .NET

*Teacher* : Christian KHOURY

*Mail* : christian.khoury@efrei.net

*Composition* : 3 lectures & 7 labs (4/7 for the project)

## What will we see ?

1. Data types
2. Condition and iteration
3. Composed types (struct, classes)
4. Classification (interface)
5. Method linking : dynamic vs static

And libraries :

* I/O librarie
* Threading
* Networking
* Graphics

## Main features of C##

Very similar to Java (work on Virtual Machine). 

C-based syntax but object base class. 

No pointers, objetc parameters are references.

All code in classes.



Rappel static attributes and methods :

```c#
class A {
    static void meth(){
        //this method is for all the instance. It is a static method !
    }
    
    void meth(){
        //this method point on the instance of the object who "invok" it.
    }
}
```

## Delegates & Events

Delegation classes : used in C# to implement pointers to methods. Useful for event handling and event programming. A delegation class inherits from the Delegate class in a special way.







