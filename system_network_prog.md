# System and Network programming

*Teachers*  : Dario Vieira, Cristiano Rodrigues

*Mails* : dario.vieira@efrei.fr

## System programming

Need to know : Network stacks (application/....)

### Goals

* Unix programming
* Various command line utilities
* Files, processes, memory management
* System calls, networking and concurrency

### Books

* Modern Operating Systems
* Advanced Programming in the Unix Environment
* Computer Networking : A top-Down Approach

### Modern computing

Why is the problem if I work into the hardware layer ? Problem = hardware broken

Type of OS :

* Monolithic Operating System
* Layered Operating System
* Microkernels
* Client/Server model

How does OS get control ?

* Process Management
* File Management
* Directory Management
* Miscellaneous





