# DevOps and continuous delivering

Jacques AUGUSTIN

PART 1 : The fundamentals : important concepts and definitions

PART 2 : DevOps, a conceptual approach

PART 3 : DevOps in practice

# PART 1 : Fundamentals 

Software : a sequence of instructions given to a computer. 

IT ecosystem is complex, impossible to a human to know all of the concepts.

## Information sytem

How do you define an information sytem ?  

 * The core system
 * The sattelites systems
 * The connectors

*Satellites are connected to the core by connectors.*

Components of an information system :

* Software
* People
* Protocols
* Networks
* Servers
* Hardware
* Databases

To link the components :

* Workflow : a sequence of actions determinates by a set of rules.
* Processes

An information system is all of the components see above linked by connections like workflow and processes.

----

Sotware :

* Program
* code or source code
* application
* solution

Web application :

- remote
- Internet

**ERP = Enterprise Resource Planning** (very important to know)

---

TRM AS400 : can handle an extremly long number of transacs per second. Use in banks, insurances, health care, CNAF etc.

## Information Technology Delivery

IT steps for a project :

```mermaid
graph LR
A[Plan] -->B[Analyse]
B-->C[Design]
C-->D[Build]
D-->E[Test]
E-->F[Deploy]
```

A **release is a deployable software package**. A release manager is in charge of the integrity and the agreed quality of the release.

### IT Environment

```mermaid
graph LR
A[Computer] -->B(Server)
A -->C(Workstation)
```

```mermaid
graph LR
A[USER]-->B[Program]
B-->F(Dependencies : Drivers, libraries...)
B-->C[Operating system]
C-->D[CPU]
```

Different type of environement :

* Development
* Integration
* User Acceptance Tests
* Performance
* Pre-production
* Production

*There are others environements !*

Each environement has different dependencies, maybe different OS and different needs. So more is the number of environement more is the cost of the development.

One environement for each "quality space". Exemple : "quality space" DEV can do diffrent things than "quality space" TESTS. To change the environement, we need to exchange recourses (**like** code) from environement A to environement B. This deal need to change and prepare those resources. That cost a lot. 

## IT infrastructure

Infra refers to the composite hardware and network.

## Projects

* Purpose (goal)

* Work

* Solution

* Teams
* Planning
* Budget

```
PROJECT DEFINITION:
A project is the combination of resources, goals, planning and budget all linked to an information system.
```

### The deployment pipeline

```mermaid
graph LR
A[CODE]-->B[BUILD]
B-->C[TEST]
C-->D[DEPLOY]
D-->E[MAINTENANCE]
```

*"Build" it is to generate an executable (runnable) "file"*.



There are different roles into a project :

* Project manager 
* Release manager
* Product owner
* Developers
* Testers
* Operators

## Quality

The product or the customer meets the requirements. THAT IT'S THE QUALITY.

Quality assurance. The quality is bring by the end user (the customers !)

## Methodology

The pipeline, the waterfall and the V-Model.

* deployment pipeline : see above
* waterfall 
* V-Model : define all before develpment

**The big misunderstanding** : the IT departement tends to ignore, no listen and bypass the other and what the business world says.

# 